---
layout: post
title: "The 5 year story"
description: "The 5 year story"
keywords: "Life"
---
![img1](../../../../img/img2.jpg)

Today will be the last class we will be attending in this 5yr of college life. There surely isn’t an yearning in me for losing classes from now because I got very lazy and disinterested in those “literally speakings” after I failed for a mere adaptation.

This is how the 5 year academics went

* ### Year 1
I somehow have got in. I have the pride that I am in Anna Univ. I am punctual and attend all the classes. I am good at programs. I hate maths. I hate books and studying. I can’t learn the 12th subjects again. Arrears…

* ### Year 2
I have to clear the arrears I got. I will cut classes if its boring. I am awake till 3 am for no reasons. I started bunking the first hours. Internals went down, performance still degraded. Studied even the well versed subjects with disinterest. Arrears again…

* ### Year 3
Could identify how partial staffs are. Now I have a laptop and I can watch films, browse everyday. I am afraid and I can’t present well before my class. Now I am a cat on the wall, struggled for mere pass marks. Even then I am assault. Whether its an internals or the main sem I can attend it just by preparing for 1 or 2 hours. I am not worried about the grades. Arrears retained…

* ### Year 4
I am with a project that I am familiar with. I could learn more when I did it myself. I got a new interest in ‘web design’. I knew my level and how I performed as intern. I can’t take up the simple tactics and the wicked tricks to fluorish. Somehow I have cleared my arrears…

* ### Year 5
I am worried about grades affecting placements. This time its surely luck when I ain’t fully prepared for a recruitment and I am into a job. The ultimatum of college life achieved, next what… ‘Don’t be reluctant and get into trouble. Be watchful and get away silently’…

Everyday here throttled between “lost chances” and “last chances” right from beginning. Apart from academics opportunities and chances were more ‘I could have lofted a six, swam a freestyle, twisted a dance, addressed a gathering, wrote in french, went an outing, attended a symposium’ etc, etc… There only is regrets for all those missed…
