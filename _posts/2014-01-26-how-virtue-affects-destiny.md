---
layout: post
title: "How Virtue affects Destiny"
description: "How Virtue affects Destiny"
keywords: "Life"
---

![img1](../../../../img/img3.jpg)

Today when am spending times, I have got a very big doubt “Am I really destined for what I am now ?”

I have always been optimistic finding no real difference between these 2 paradigms :
" Blame yourself for what you are and praise yourself for what you are not."<br/>
" Praise yourself for what you are and blame yourself for what you are not. "

### DESTINY
Initially I believed it was all spontaneous and everything happened according to destiny. I have been into a very clueless life, every part driven by unconditional chances and coincidences. No goal, no determination and no special efforts. But I have been placed to travel through a comfortable path by destiny. I believed that the good deeds and the prayers my parents have showered on my cause have always helped.

### VIRTUE
My parents have greatly inspired me with one-thing ie, ‘Compromises’. All the learning in these years seeing them sacrifice most of their happiness for our wellness is galvanized in my blood. They always told me that making a lot of compromises in a belief that there are good times ahead will surely workout for some good cause someday. Apart from these inspirations my patience should be the best virtue I have got. I have learnt to wait and wait and regret less for all those lost chances and in the meanwhile I had the good chances.

Besides this I was thinking(but not regret) that I was unfortunate, because I have not enjoyed many things that other people in my surroundings are enjoying at this stage. This substantial thought never allowed to realize that I am already happy the way I am living and is asking for more..
