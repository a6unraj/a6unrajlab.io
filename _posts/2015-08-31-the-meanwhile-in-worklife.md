---
layout: post
title: "The meanwhile in work life…."
description: "The meanwhile in work life…."
keywords: "Life"
---

![img1](../../../../img/img4.jpg)

I thought moving to Chennai will provide me a comfortable work atmosphere. I got into a project that has made me very irregular.
My casualties continued from college life. Procrastination has again been the hindrance. Initially I was not mindful of my role, which greatly slowed me in learning my responsibility at work. Eventually my tasks turned a heck. My project manager was a genuine person and he allowed me to be on my ways and did not utter a word about my performance. But only until we were into the scorching phases of the project, he then induced me to put some extra effort and get going better. The learning process in work life has been too slow but has made a great transition. Just then a small realization arouse me ‘when reputation is lost everything is lost’ and that brought the fear. With a small sacrifice in time I did a lot of self learning and put in efforts, then came the transition phase that has unconditionally groomed me. I noticed lots of changes in personal behavior, character and ways of life in this transition.

> I believe this has ‘literally’ started bringing in me the real maturity and adulthood has just now begun !
