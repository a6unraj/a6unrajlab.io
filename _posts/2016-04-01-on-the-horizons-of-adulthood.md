---
layout: post
title: "On the Horizons of Adulthood"
description: "On the Horizons of Adulthood"
keywords: "Life"
---

Must be the hardest stage of life,
When at around 25 I may be proud that I am sailing well on my own, both professionally and personally. But wait the people in my surroundings won’t allow me to decide upon what I want and settle upon what I have.

![img1](../../../../img/img5.jpg)

Though the action is all mine I am ultimately tamed by the contagious desires fed by the flaunting acquaintances. Most of us just take the unconditional chances not knowing that we are capable of taking ‘the road not taken’. Lucky are those who constantly get the right advice at the right time and took the right action at all stages of life.

![img1](../../../../img/img6.jpg)

First hardest thing is about deciding upon the job we hold. Its all that decides the way of living; Where to live, when to live and how to live. The world is getting very competitive everyday and the fear that the struggle for the existence is getting tougher makes the urge to constantly learn new things. And with the pace of this urge I should be constantly trying to get a better job than the current one, else I am lost to fragility. The first hope for a healthy situation here in a job is getting a satisfaction of accomplishment or some recognition for the work. Second is going in harmony with peers, building reputation or fame and having a cheerful workplace. The key factors for the success here are self-discovery, affirmation and acceptance.

![img1](../../../../img/img7.jpg)

Second hardest thing is deciding about the friends, family, relationships and the time spent with them. We will have to decide carefully whom to stay with, whom to hangout with, whom to go close with and also importantly whom to ignore. Keep an eye on who’s giving pace for our values and get closer with them. Recognize who really cares and start socializing at the premium with those who are just curious. Keep parents, siblings and family happier by spending more time with them. The healthy situation here is having good friends in close circles apart from family, to get a feel that we are not isolated and limited.

I have just now grown to a stage where I could define all the above and I wish that I get into those healthy situations spontaneously.
